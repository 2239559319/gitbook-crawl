const { XMLParser } = require('fast-xml-parser');
const fs = require('fs-extra');
const { join, resolve, dirname } = require('path');

const outDist = resolve('./dist');

async function getFilePath({ type, url }) {
  const u = new URL(url);
  const { pathname } = u;
  // html
  if (type === 'html') {
    const desPath = join(outDist, pathname);
    await fs.mkdirp(desPath);
    const filePath = join(desPath, 'index.html');
    const e = await fs.exists(filePath);
    return {
      filePath,
      exists: e,
      async write(content) {
        if (!e) {
          await fs.writeFile(filePath, content, { encoding: 'utf-8' });
        }
      }
    };
  } else if (type === 'image') {
    const desPath = join(outDist, pathname);
    await fs.mkdirp(dirname(desPath));
    const e = await fs.exists(desPath);
    return {
      filePath: desPath,
      exists: e,
      async write(content) {
        if (!e) {
          await fs.writeFile(desPath, content, { encoding: 'binary' });
        }
      }
    };
  } else if (type === 'css' || type === 'js') {
    const desPath = join(outDist, pathname);
    await fs.mkdirp(dirname(desPath));
    const e = await fs.exists(desPath);
    return {
      filePath: desPath,
      exists: e,
      async write(content) {
        if (!e) {
          await fs.writeFile(desPath, content, { encoding: 'utf-8' });
        }
      }
    };
  }
}

async function getHtmlUrl(url) {
  const sitemapUrl = `${url}/sitemap.xml`;

  const res = await fetch(sitemapUrl, {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
    }
  });
  const text = await res.text();

  const parser = new XMLParser();
  const json = parser.parse(text);
  console.log('sitemap done');
  return json.urlset.url.map(({ loc }) => loc);
}

async function downloadHtml(url) {
  const res = await fetch(url, {
    headers: {
      Accept: 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
      'Accept-Language': 'zh-CN,zh;q=0.9',
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
    }
  });

  const text = await res.text();

  const replaceUrl = [];

  const out = text.replace(/"(https?:\/\/[-\w]+?\.gitbook\.(com|io)(\/.+?))"/g, (match, $1, $2, $3) => {

    if ($1.includes('cdn-cgi')) {
      const path = `/cdn-cgi/${Math.random().toString(36).slice(2)}.avif`;
      replaceUrl.push({
        url: $1,
        replace: path
      });

      return `"${path}"`;
    } else {
      replaceUrl.push({
        url: $1,
        replace: $3
      });
      return $3;
    }
  });
  const { write } = await getFilePath({ url, type: 'html' });
  await write(out);
  console.log(`${url} done`);
  return replaceUrl;
}

async function downloadStatic(url, staticUrls, replace) {

  try {
    const res = await fetch(url, {
      headers: {
        Accept: '*/*',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36'
      },
    });
  
    const contentType = res.headers.get('Content-Type');
    const { pathname } = new URL(url);
    if (contentType.includes('avif')) {
      const buf = await res.arrayBuffer();
      const { write } = await getFilePath({ type: 'image', url: `https://baidu.com${replace}` });
      await write(Buffer.from(buf));
    } else if (contentType.includes('image') || contentType.includes('font')) {
      const buf = await res.arrayBuffer();
      const { write } = await getFilePath({ type: 'image', url })
      await write(Buffer.from(buf));
    } else if (contentType.includes('css')) {
      const text = await res.text();
      const { write } = await getFilePath({ type: 'css', url });
      await write(text);
    } else if (contentType.includes('javascript') || pathname.endsWith('.js')) {
      const text = await res.text();

      const out = text.replace(/"(https?:\/\/[-\w]+?\.gitbook\.(com|io)(\/.+?))"/g, (match, $1, $2, $3) => {
        staticUrls.push({
          url: $1,
          replace: $3
        });
        return `"${$3}"`;
      });
      const { write } = await getFilePath({ type: 'js', url });
      await write(out);
      console.log(`${url} done`);
    }
  } catch (error) {
    console.info(url);
  }
}

;(async() => {
  const htmlUrls = await getHtmlUrl('https://2239559319.gitbook.io/jing-tong-vue3-yuan-ma');
  const staticUrls = [];
  for (const url of htmlUrls) {
    staticUrls.push(...(await downloadHtml(url)));
  }

  const set = new Set();

  for (let i = 0; i < staticUrls.length; i++) {
    const { url, replace } = staticUrls[i];
    if (!set.has(url)) {
      set.add(url);
      await downloadStatic(url, staticUrls, replace);
    }
  }

})();

// ;(async() => {
//   const res = await fetch('https://app.gitbook.com/public/fonts/IBMPlexMono/IBMPlexMono-Bold.woff2?v=3.21', {
//     headers: {
//       Accept: '*/*',
//     }
//   });
//   const buf = await res.arrayBuffer();
//   await fs.writeFile('./out.png', Buffer.from(buf), 'binary');
// })();